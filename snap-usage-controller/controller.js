

var Controller = function () {
}

Controller.prototype.getViewModel = function(data) {
	if (data.serviceId == "0411111111") {
		return mockData1;
	} else {
		return mockData2;
	}
	
}

module.exports = Controller;

var mockData1 = {
	template : "usage/mobile",
	model: {
		serviceId : "0411111111",
		usage :{
		    data: {
		      used: 888000,
		      allocated: 1000000
		    },
		    voice: {
		      used: 600,
		      allocated: 1000
		    }
		}
	}
};

var mockData2 = {
	template : "usage/mobile",
	model: {
		serviceId : "0488888888",
		usage :{
		    data: {
		      used: 50000,
		      allocated: 1000000
		    },
		    voice: {
		      used: 500,
		      allocated: 1000
		    }
		}
	}
};