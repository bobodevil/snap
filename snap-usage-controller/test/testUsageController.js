var request = require('superagent');
var expect = require('expect.js');
var Controller = require('../controller');


before(function(){
	  //TODO seed the database
});


describe('Mobile Usage Suite ', function(){
	it ("Testing 0411111111", function(done){
		var controller = new Controller();
		var objectOutput = controller.getViewModel({serviceId: "0411111111"});

		expect(objectOutput).to.exist;
		expect(objectOutput.model.serviceId).to.equal("0411111111");
		expect(objectOutput.model.usage.data.used).to.equal(888000);
		done();
		
	});
	it ("Testing 0488888888", function(done){
		var controller = new Controller();
		var objectOutput = controller.getViewModel({serviceId: "0488888888"});

		expect(objectOutput).to.exist;
		expect(objectOutput.model.serviceId).to.equal("0488888888");
		expect(objectOutput.model.usage.data.used).to.equal(50000);
		done();
		
	});
});