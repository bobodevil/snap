
var tagRegExp = /<div[^>]+class=['"]snap-widget['"][^>]*\/>/g;
var idAttributeRegExp = /data-widget-id=['"]([^'"]+)['"]/;
var preloadAttributeRegExp = /data-widget-preload=['"]([^'"]+)['"]/;
var paramsAttributeRegExp = /data-widget-params=(?:'([^']+)'|"([^"]+)")/;

module.exports = ChunkedPage;

function ChunkedPage (html) {
    this.chunks = [];

    var text_chunks = html.split(tagRegExp);
    var widget_chunks = html.match(tagRegExp);

    this.hasWidgets = (widget_chunks && widget_chunks.length > 0);

    for (var i = 0; i < text_chunks.length; i++) {
        this.chunks.push(parseTextChunk(text_chunks[i]));
        if (widget_chunks && i < widget_chunks.length) {
            this.chunks.push(parseWidgetChunk(widget_chunks[i]));
        }
    }

    return this;
}

ChunkedPage.prototype.reconstitute = function() {
    var html = '';
    for (var i = 0; i < this.chunks.length; i++) {
        html += this.chunks[i].content;
    }
    return html;
};

ChunkedPage.prototype.hasMoreWidgets = function() {
    for (var i = 0; i < this.chunks.length; i++) {
        if (this.chunks[i].content.indexOf("data-widget-id") > -1) {
        	return true;
        }
    }
    return false;
};

// Private functions below here

function parseTextChunk (chunk) {
    return {
        type: 'text',
        content: chunk
    }
}

function parseWidgetChunk (chunk) {
    var result = {
        type: 'widget',
        content: chunk,
        preload: true
    };

    // The widget ID
    var match = chunk.match(idAttributeRegExp);
    if (match) {
        result.id = match[1];
    }

    // Whether or not to preload the widget, defaults to true
    match = chunk.match(preloadAttributeRegExp);
    if (match) {
        result.preload = (match[1] == 'true');
    }

    // Params are a JSON string within an attribute. If the attribute value is within
    // double quotes ("), the double quotes within the JSON string will be encoded as &quot;
    match = chunk.match(paramsAttributeRegExp);
    if (match) {
        result.params = JSON.parse((match[1] || match[2]).replace(/&quot;/g, '"'));
    }

    return result;
}

