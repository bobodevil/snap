var http = require('http');

var PageTemplateProvider = require('./PageTemplateProvider');


var pageTemplateProvider = new PageTemplateProvider();
var util = require('util');
var colors = require('colors');



var welcome = [
  ' ####  #     # #####    #####  ',
  '#      ##    # #    #   #    # ',  
  ' ###   # #   # #    #   #    # ',   
  '     # #  #  # # ## #   #####  ',   
  '     # #   # # #    #   #      ',   
  '#####  #    ## #    #   #      '
].join('\n');
util.puts(welcome.yellow.bold);





http.createServer(pageTemplateProvider.getRoute()).listen(8889);

util.puts('http proxy server'.white + ' started '.green.bold + 'on port '.white + '8889'.yellow);
util.puts('http CMS server '.white + 'started '.green.bold + 'on port '.white + '4502 '.yellow);