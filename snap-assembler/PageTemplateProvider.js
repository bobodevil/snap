var ChunkedPage = require('./ChunkedPage');
var Renderer = require('snap-renderer');
var router = require('router');
var http = require('http');
var fs = require('fs');
var path = require('path');

var requestPool = new http.Agent();
requestPool.maxSockets = 1000000 ;

var config = {
	controllers : {
		usage : {
			module : "snap-usage-controller"
		}
	}

}


var pages = {
	    "/": "<html>Hello world!</html>\n",
	    "/myaccount": "<html><head><link rel='stylesheet' href='/styles/stylesheets/doc_optus.css'/><title>My Account</title></head><body><h2>Usage</h2><div class='snap-widget' data-widget-id='usage'/></body></html>\n",
};

function getTemplate(req, res) {

	retrieveStaticTemplate(req, res, req.url);
	
}

function serveStaticFileFromStyleGuide(req, res, url) {
	var mimeTypes = {
    "html": "text/html",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "png": "image/png",
    "js": "text/javascript",
    "css": "text/css"};

	var filename = path.join(process.cwd(), "../" + url);
    path.exists(filename, function(exists) {
        if(!exists) {
            console.log("not exists: " + filename);
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write('404 Not Found\n');
            res.end();
            return;
        }
        var mimeType = mimeTypes[path.extname(filename).split(".")[1]];
        res.writeHead(200, {'Content-Type':mimeType});

        var fileStream = fs.createReadStream(filename);
        fileStream.pipe(res);

    });

}
function retrieveStaticTemplate(req, res, path) {
	callback(req, res, pages[path]);
}

function isStaticAssets(path){
	//Refactor This to multi 
	
	if (path.indexOf(".jpg") > -1) {
		return true;
	}
	if (path.indexOf(".png") > -1) {
		return true;
	}
	if (path.indexOf(".gif") > -1) {
		return true;
	}
	if (path.indexOf(".js") > -1) {
		return true;
	}
	if (path.indexOf(".css") > -1) {
		return true;
	}
	if (path.indexOf(".woff") > -1) {
		return true;
	}
	return false;
}




function retrieveFromTemplatingEngine(req, res, path) {
	


	var options = {
		host: 'localhost',
		path: path,
		port: '4502',
		headers: req.headers,
		method: req.method ,
		agent:requestPool
	};
	
	
	callbackRequest = function(response) {
		res.writeHead(response.statusCode, response.headers);
		
		var staticAssetTest = isStaticAssets(path);

		var str = ''
		response.on('data', function (chunk) {
			if (staticAssetTest) {
				res.write(chunk);
			} else {
				str += chunk;
			}
			
		});
		response.on('end', function () {
			if (staticAssetTest){
				res.end();
			} else {
				callback(req, res, str);
			}
		});
	}

	var proxy_req = http.request(options, callbackRequest);


	if (proxy_req.method == "POST" || proxy_req.method == "PUT") {
		proxy_req.body = '';

        req.addListener('data', function(chunk) {
            proxy_req.write(chunk);
        });

        req.addListener('end', function() {
            proxy_req.end();
        });
	} else {
		proxy_req.end();
	}
	
	
	
}


function callback(req, res, response) {
	
	//console.log(renderer.renderTemplate({},{}));
	
	
	//console.log(response);
	if (! response) {
		res.end();
        return;
    }
    var page = new ChunkedPage(response);
    if (! page.hasWidgets) {
    	res.end(response);
        return;
    }
    for (var i = 0; i < page.chunks.length; i++) {
        var chunk = page.chunks[i];
        
        if (chunk.type == 'widget') {
        	console.log(chunk.id);
            //console.log('have to instantiate: ' + JSON.stringify(chunk));
            var conf = config.controllers[chunk.id];
            var module = conf.module;

            console.log(module);
            
            //var params = conf.default_params;
            var params = {};

            sendModuleRequest(req, module, params, page, i, res);
        }
    }
} 


function sendModuleRequest(req, module, params, page, i, res) {
	var renderer = new Renderer();
	

	var Controller = require(module);

	var controller = new Controller();

	var viewModel = controller.getViewModel(params);


	//To remove


	//var stringHtml = JSON.stringify(viewModel);
	
	var html = renderer.renderTemplate(viewModel.model, viewModel.template);

	page.chunks[i].content = html;

	if (!page.hasMoreWidgets()) {
		res.end(page.reconstitute());
	}

	/**vertx.eventBus.send(addr, params, function(response) {
        var html = renderer.renderTemplate(response.model, response.template);
        page.chunks[i].content = html;
        
        //TODO check has more widgets
        if (!page.hasMoreWidgets()) {
        	req.response.end(page.reconstitute());
        }
    });**/
}

function getRoute() {
	var route = router();
	route.all('/', function(req, res) {
		retrieveStaticTemplate(req, res, req.url);
	});
	route.all('/myaccount', function(req, res) {
		retrieveStaticTemplate(req, res, req.url);
	});
	route.all('/styles/*', function(req, res) {
		serveStaticFileFromStyleGuide(req, res, req.url);
	});
	route.all('/*', function(req, res) {
		retrieveFromTemplatingEngine(req, res, req.url);
	});
	return route;
}

function PageTemplateProvider() {
	this.getTemplate = getTemplate;
	this.getRoute = getRoute;
	return this;
};


module.exports = PageTemplateProvider;
