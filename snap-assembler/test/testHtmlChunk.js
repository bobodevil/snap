var request = require('superagent');
var expect = require('expect.js');
var PageTemplateProvider = require('../PageTemplateProvider');


before(function(){
	  //TODO seed the database
});




describe('Test Page ', function(){
	it ("Testing 1", function(done){
		var pageTemplateProvider = new PageTemplateProvider();
		
		var mockReq = {
			url : "/"
		}
		
		var output = pageTemplateProvider.getTemplate(mockReq);
		
		console.log(output);
		
		var htmlResult = "test";
		expect(htmlResult).to.exist;
		expect(htmlResult).to.equal("test");
		done();
		
	});
	it ("Testing 2", function(done){
		var pageTemplateProvider = new PageTemplateProvider();
		
		var mockReq = {
			url : "/myaccount"
		}
		
		var output = pageTemplateProvider.getTemplate(mockReq);
		
		console.log(output);
		
		var htmlResult = "test";
		expect(htmlResult).to.exist;
		expect(htmlResult).to.equal("test");
		done();
		
	});
});