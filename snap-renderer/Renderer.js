
var React = require("react");


function Renderer() {

	//var baseDir = config.baseDir;
	
	this.renderTemplate = function(data, templateName) {
		var baseDir = "./styles/patterns/"
			
		var propStore = "<script> var stringData = '" + JSON.stringify(data) + "'; \n var model = JSON.parse(stringData); </script>";
	
		var reactObject = require(baseDir + templateName);
	
		var component = reactObject({data:data});
		// console.log(data);	

		var widgetContent = React.renderComponentToStaticMarkup(component);
		
		// var widgetContent = "<div>widget content</div>";
			
		return propStore + widgetContent;
	}
	
}

module.exports = Renderer;