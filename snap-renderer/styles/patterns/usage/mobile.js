/**
 * @jsx React.DOM
 */
var React = require('react/addons');
var ProgressBar = require('../../components/progress_bar/progress_bar')

var MobileUsagePattern = React.createClass({displayName: 'MobileUsagePattern',

	// TODO: propTypes: function() { }

 getInitialState: function() {
    return {
    	usage: this.props.data.usage,
    	serviceId: this.props.data.serviceId
    };
  },

  bytesToSize: function(bytes) {

       var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
       if (bytes == 0) return '0 Byte';
       var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

       return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
  },

  formatServiceNumber: function(number) {

    return number.slice(0,4) + ' ' + number.slice(4, 7) + ' ' + number.slice(7, 10);

  },

  render: function() {
  	      	var percent = this.props.data.usage.data.used * 100 / this.props.data.usage.data.allocated;
  	      	var message = this.bytesToSize(this.props.data.usage.data.used) + " of " + this.bytesToSize(this.props.data.usage.data.allocated);
            var leftOver = this.bytesToSize(this.props.data.usage.data.allocated - this.props.data.usage.data.used);

            var tone = 'success';
            if(percent > 80) {
              tone = 'alert';
            } else if(percent > 50) {
              tone = 'warn';
            }

            var minutes = Math.floor(this.props.data.usage.voice.used / 60);
            var seconds = this.props.data.usage.voice.used - (60 * minutes);

    return React.DOM.div({className: "usage mobileUsageWidget"}, 
      		React.DOM.h2(null, this.formatServiceNumber(this.props.data.serviceId)), 
          React.DOM.div({className: "usage-entity"}, 
            React.DOM.h3(null, "Data Usage"), 
            ProgressBar({value: percent, text: message, tone: tone}), 
            React.DOM.p(null, "You have ", React.DOM.strong(null, leftOver), " left to use")
          ), 
          React.DOM.div({className: "usage-entity"}, 
            React.DOM.h3(null, "Voice Usage"), 
            React.DOM.span({className: "voice"}, minutes, ":", seconds), " Minutes"
          )
      )
    
  },

  refresh: function(model) {
  		this.setState({ usage: model.usage, serviceId: model.serviceId});
  }

});

module.exports = MobileUsagePattern;