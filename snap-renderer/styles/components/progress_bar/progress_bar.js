/**
 * Progress Bar Component
 * @jsx React.DOM
 */

var React = require('react/addons');

var ProgressBar = React.createClass({displayName: 'ProgressBar',
	// TODO enforce proptype?
	propTypes: {
		value: React.PropTypes.number,
		text: React.PropTypes.string,
		tone: React.PropTypes.string
	},
	render: function() {

		var progressStyle = {
			width: this.props.value + '%'
		};

		var classSet = 'progress-bar ' + this.props.tone;

		return React.DOM.div({className: "progress"}, 
			React.DOM.div({className: classSet, style: progressStyle}, React.DOM.span(null, this.props.text))
		)
	}
});

module.exports = ProgressBar;