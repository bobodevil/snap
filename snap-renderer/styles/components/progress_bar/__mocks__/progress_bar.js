/**
 * @jsx React.DOM
 */
var React = require.requireActual('react/addons');
var ProgressBar = React.createClass({displayName: 'ProgressBar',

  render: function() {
    return React.DOM.div({className: "progressBarMock", 'data-tone': this.props.tone, 'data-value': this.props.value, 'data-text': this.props.text})
  },

});

module.exports = ProgressBar;
