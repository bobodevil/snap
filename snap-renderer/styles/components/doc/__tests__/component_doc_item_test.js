/**
 * @jsx React.DOM
 */
'use strict';

 jest.dontMock('../doc_item');

 var DocItem = require('../doc_item');
 var use_case1 = require('../use_cases/sample_component.json');

 // Mock change use case fn
 // FIXME implement spy instead (couldnt get the spy to work with JEST)
 var changeUseCaseCallCount = 0;
 use_case1.changeUseCase = function() {
 	changeUseCaseCallCount++;
 };


var React = require('react/addons');
var TestUtils = React.addons.TestUtils;


describe('Documentation: component item', function() {

	var instance = DocItem(use_case1);
	
	TestUtils.renderIntoDocument(instance);


	it('Should have an "active" class', function() {
		expect(instance.getDOMNode().className.indexOf('active')).not.toBe(-1);
	});

	it('Should have a title of "Progress Bar"', function() {
		var title = TestUtils.findRenderedDOMComponentWithTag(instance, 'h2');
		expect(title.getDOMNode().textContent).toBe('Progress Bar');
	});

	it('Should have a list with 2 use cases', function() {
		var list = TestUtils.findRenderedDOMComponentWithTag(instance, 'ul');
		expect(list.getDOMNode().children.length).toBe(2);
	});

	it('Should trigger an update action when a use case is selected', function() {
		var listItems = TestUtils.scryRenderedDOMComponentsWithTag(instance, 'a');
		var firstUseCaseNode = listItems[0].getDOMNode();
		React.addons.TestUtils.Simulate.click(firstUseCaseNode);

		expect(changeUseCaseCallCount).not.toBe(0);

	});

	it('Should display the JSX component', function() {
		var componentTextCode = TestUtils.findRenderedDOMComponentWithClass(instance, 'doc-component-jsx');
		// It should have a tag..
		expect(componentTextCode.getDOMNode().textContent.indexOf('<')).not.toBe(-1);

	});

});

