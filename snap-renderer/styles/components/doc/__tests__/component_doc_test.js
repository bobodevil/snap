/**
 * @jsx React.DOM
 */
'use strict';

jest.dontMock('../doc');

var Doc = require('../doc');
var useCase = require('../use_cases/sample_component_list.json');

var React = require('react/addons');
var TestUtils = React.addons.TestUtils;


describe('Documentation: wrapper', function() {

	var instance = Doc(useCase);
	// console.log(React.renderComponentToString(instance));
	
	TestUtils.renderIntoDocument(instance);

	it('should display a list of 2 style components', function() {

		var items = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'doc-component-item');
		expect(items.length).toBe(2);

	});

	it('should display a Live View window', function() {

		var liveView = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'doc-live-view');
		expect(liveView.length).toBe(1);
	});

});