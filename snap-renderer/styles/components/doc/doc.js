/**
 * Documentation component
 * @jsx React.DOM
 */

var React = require('react/addons');

var DocItem = require('./doc_item');
var DocLiveView = require('./doc_live_view');
var components = require('../../index');

var Doc = React.createClass({displayName: 'Doc',

	getInitialState: function() {
		return {
			selectedComponentIndex: 0,
			selectedUseCaseIndex: 0
		}
	},

	propTypes: {
		items: React.PropTypes.array
	},

	// Method to change currently state of use case
	changeUseCase: function(component_index, case_index) {

		this.setState({
			selectedComponentIndex: component_index,
			selectedUseCaseIndex: case_index
		});

	},

	// FIXME HACK!
	setBrand: function() {

		var optusCurrent = document.location.pathname.indexOf('optus') !== -1;

		if(optusCurrent) {
			document.location = '/docs/singtel.html';
		} else {
			document.location = '/docs/optus.html';
		}

	},

	render: function() {


		// Dynamic require on live view item
		var liveViewComponent = components[this.props.items[this.state.selectedComponentIndex].path];
		var liveViewItem = liveViewComponent(this.props.items[this.state.selectedComponentIndex].use_cases[this.state.selectedUseCaseIndex]);

		var propsText = '<Component ';

		if(liveViewItem) {
			for(var i in liveViewItem.props) {
				propsText = propsText + i + '="' + liveViewItem.props[i] + '" ';
			};
			propsText += '/>';			
		}

		console.log(this.props.items[this.state.selectedComponentIndex].use_cases[this.state.selectedUseCaseIndex]);

		var itemList = this.props.items.map(function(item, i) {
			return DocItem({
						key: i, 
						active: i === this.state.selectedComponentIndex, 
						title: item.title, 
						use_cases: item.use_cases, 
						example: propsText, 
						changeUseCase: this.changeUseCase, 
						selectedUseCase: this.state.selectedUseCaseIndex}, 
							liveViewComponent(this.props.items[this.state.selectedComponentIndex].use_cases[this.state.selectedUseCaseIndex])
						);
		}, this);

		// TODO integrate with JSX layout (SNAP-13)
		return React.DOM.div(null, 
			React.DOM.div({className: "doc-header"}, 
				React.DOM.div({className: "container"}, 
					React.DOM.div({className: "row"}, 
						React.DOM.div({className: "col-xs-12"}, 
							React.DOM.a({onClick: this.setBrand.bind(null), className: "logo"}, " "), 
							React.DOM.h1(null, "Interactive Guide")
						)
					)
				)
			), 
			React.DOM.div({className: "container doc-styles"}, 
				React.DOM.div({className: "row"}, 
					React.DOM.div({className: "col-xs-12 col-sm-4"}, 
						itemList
					), 
					React.DOM.div({className: "col-xs-12 col-sm-8"}, 
						DocLiveView(null, 
							liveViewItem
						)
					)
				)
			)
		)

	}
});

module.exports = Doc;