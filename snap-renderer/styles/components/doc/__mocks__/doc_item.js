/**
 * Mock for doc item
 * @jsx React.DOM
 */
'use strict';

var React = require('react/addons');

module.exports = React.createClass({displayName: 'exports',
	render: function() {
		return React.DOM.div({className: "doc-compownent-item"}, this.props.title)
	}
})