/**
 * Documentation component item display
 * @jsx React.DOM
 */

var React = require('react/addons');

var DocItem = React.createClass({displayName: 'DocItem',

	propTypes: {
		title: React.PropTypes.string,
		use_cases: React.PropTypes.array,
		selectedUseCase: React.PropTypes.number,
		example: React.PropTypes.string,
		active: React.PropTypes.bool
	},

	render: function() {

		var changeUseCase = this.props.changeUseCase;

		// Generate list items for each use case
		var useCases = this.props.use_cases.map(function(use_case, i) {
			// Apply selected flag
			var selected = (i === this.props.selectedUseCase && this.props.active) ? 'selected': '';

			return React.DOM.li({key: i}, " ", React.DOM.a({onClick: changeUseCase.bind(null, this.props.key, i), className: selected}, "Use Case ", i + 1));
		}, this);

		// Apply active class to container
		var classSet = React.addons.classSet({
			'doc-component-item': true,
			'active': this.props.active
		});

		return React.DOM.div({className: classSet}, 
          React.DOM.h2({onClick: changeUseCase.bind(null, this.props.key, 0)}, this.props.title), 
          React.DOM.ul({className: "doc-component-use-cases"}, 
            useCases
          ), 
          React.DOM.div({className: "doc-component-jsx"}, 
            this.props.example
          ), 
          React.DOM.div({className: "doc-live-view-compact"}, 
          	this.props.children
          )
        )
	}
});

module.exports = DocItem;