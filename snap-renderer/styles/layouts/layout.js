/**
 * Two column responsive layout
 * @jsx React.DOM
 */
 
var React 	= require('react/addons');
var Column 	= require('./column');

/**
 * Layout using the column component
 */
var Layout = React.createClass({displayName: 'Layout',
	
	propTypes: {
		fluid		: React.PropTypes.bool,
		children	: React.PropTypes.arrayOf(Column).isRequired
	},

	getDefaultProps: function() {
		return {
			fluid		: false,
		};
	},
	
	// Render the object
	render: function() {
		var rowClass = 'row'; 
		
		return this.transferPropsTo(
			React.DOM.div({className: this.props.fluid ? 'container-fluid' : 'container'}, 
				React.DOM.div({className: rowClass}, 
					this.props.children
				)
			)
        ); 	
	}	
});

module.exports = Layout;