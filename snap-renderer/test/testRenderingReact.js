var request = require('superagent');
var expect = require('expect.js');
var Renderer = require('../Renderer');


before(function(){
	  //TODO seed the database
});

var mockData = {
	template : "usage/mobile",
	model: {
		serviceId : "0488888888",
		usage :{
		    data: {
		      used: 50000,
		      allocated: 1000000
		    },
		    voice: {
		      used: 500,
		      allocated: 1000
		    }
		}
	}
};


describe('Rendering React Suite with Mobile Data', function(){
	var renderer = new Renderer();
	var output = renderer.renderTemplate(mockData.model, mockData.template);
	var htmlResult = output;

	it ("React Testing is not empty", function(done){
		expect(htmlResult).to.exist;
		done();
		
	});
	it ("React Testing has HTML", function(done){
		expect(htmlResult).to.contain("<div");
		done();
		
	});
	it ("React Testing is correct mobile usage Template", function(done){
		expect(htmlResult).to.contain("usage mobileUsageWidget");
		done();
		
	});
	it ("React Testing is the correct number", function(done){
		expect(htmlResult).to.contain("0488888888");
		done();
	});
});