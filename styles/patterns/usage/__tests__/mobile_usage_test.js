/**
 * @jsx React.DOM
 */
jest.dontMock('../mobile');

var	MobileUsagePattern = require('../mobile');
var React = require('react/react');
var TestUtils = React.addons.TestUtils;

var mock_data_33percent = require('../__use_cases__/33percent');

describe('Mobile Usage', function() {

	var instance = MobileUsagePattern({data: mock_data_33percent});

	TestUtils.renderIntoDocument(instance);


	// Check that we have an H2 title with expected text
	it('should have the {serviceId} as the title', function() {
		var title = TestUtils.findRenderedDOMComponentWithTag(instance,'h2');
		expect(title.getDOMNode().textContent).toBe('0488 888 888');
	});



	// Check that we have a mock progress bar
	it('should have the {serviceId} usage as the title', function() {
		var progressBarReactComponent = TestUtils.findRenderedDOMComponentWithClass(instance,'progressBarMock');
		expect(progressBarReactComponent).not.toBeUndefined();
		// for some stupid reason HTML dataset attribute is not availble on this DOM node
		//expect(progressBarReactComponent.getDOMNode().dataset.tone).toBe('success');

	});

	// Check that we have a mock progress bar
	it('should have the correct voice usage', function() {
		var voiceUsage = TestUtils.findRenderedDOMComponentWithClass(instance,'voice');
		expect(voiceUsage.getDOMNode().textContent).toBe('8:20');
	});

});
