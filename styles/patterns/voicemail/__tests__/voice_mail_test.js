/**
 * @jsx React.DOM
 */
jest.dontMock('../voicemailinitial');
jest.mock('jquery');

var	VoiceMailPattern = require('../voicemailinitial');
var React = require('react/react');
var TestUtils = React.addons.TestUtils;
var $ = require('jquery');


describe('Voice Mail setting enabled', function() {
	var mock_data_initial = require('../__use_cases__/voice_mail_initial_setting_enabled.json');

	var instance = TestUtils.renderIntoDocument(VoiceMailPattern(mock_data_initial));

	// Check that we have an H2 title with expected text
	it('should have the {serviceId} as the title', function() {
		 var title = TestUtils.findRenderedDOMComponentWithTag(instance,'h2');
		 expect(title.getDOMNode().textContent).toBe('0488 888 888');
	});



	// Check that we have a checked checkbox
	it('should have checked checkbox for the voice mail setting', function() {
		var voiceMailCheckbox = TestUtils.findRenderedDOMComponentWithTag(instance,'input');
		expect(voiceMailCheckbox).not.toBeUndefined();
		expect(voiceMailCheckbox.getDOMNode().checked).toBe(true);
	});

});

describe('Voice Mail setting disabled', function() {
	var mock_data_initial = require('../__use_cases__/voice_mail_initial_setting_disabled.json');

	var instance = TestUtils.renderIntoDocument(VoiceMailPattern(mock_data_initial));

	it('should have the unchecked checkbox', function() {
		var voiceMailCheckbox = TestUtils.findRenderedDOMComponentWithTag(instance,'input');
		expect(voiceMailCheckbox).not.toBeUndefined();
		expect(voiceMailCheckbox.getDOMNode().checked).toBe(false);
	});

});

describe('Voice Mail setting AJAX post', function() {
	var mock_data_initial = require('../__use_cases__/voice_mail_initial_setting_disabled.json');

	var instance = TestUtils.renderIntoDocument(VoiceMailPattern(mock_data_initial));

	it('should have the unchecked checkbox', function() {
		var postButton = TestUtils.findRenderedDOMComponentWithTag(instance,'button');
		TestUtils.Simulate.click(postButton.getDOMNode());
		var title = TestUtils.findRenderedDOMComponentWithTag(instance,'h2');
		expect(title.getDOMNode().textContent).toBe('1234 567 890');
	});

});

