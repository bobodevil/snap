/**
 * @jsx React.DOM
 */
var React = require('react/react');
//var $ = require('jquery');

var VoiceMailPattern = React.createClass({displayName: 'VoiceMailPattern',

    getInitialState: function() {
        return {
            serviceId: this.props.service_id,
            voiceMail: this.props.voice_mail_setting,
        };
    },

    handleChange: function(event) {
        this.setState({voiceMail: event.target.checked});
    },

    formatServiceNumber: function(number) {
        return number.slice(0, 4) + ' ' + number.slice(4, 7) + ' ' + number.slice(7, 10);
    },

    changeVoicemail : function() {
        var serviceId = this.state.serviceId;
        var voiceMail = this.state.voiceMail;

        var postData = {
            voice_mail_setting : voiceMail,
            service_id : serviceId
        };

        var stringData = JSON.stringify(postData);

        $.ajax({
            url: "http://localhost:8081/rest/voicemail",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: stringData,
            success: function(data) {
                this.setState ( {
                    serviceId: data.model.service_id,
                    voiceMail: data.model.voice_mail_setting,
                });
                console.log('#################################################'+  JSON.stringify(this.state));
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
        
        return;
    },

    render: function() {
        return  React.DOM.div({className: "voicemail voiceMailWidget"}, 
                    React.DOM.h2(null, this.formatServiceNumber(this.state.serviceId)), 
                    React.DOM.input({type: "checkbox", checked: this.state.voiceMail, onChange: this.handleChange}), 
                    React.DOM.br(null), 
                    React.DOM.button({type: "button", onClick: this.changeVoicemail}, "Submit")
                );
    },

    refresh: function(model) {
        this.setState({
            voiceMail: model.voice_mail_setting,
            serviceId: model.service_id
        });
    }

});
 
module.exports = VoiceMailPattern;