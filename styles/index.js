// Components/patterns map
// TODO find a way to not depend on this index

module.exports = {
	ProgressBar: require('./components/progress_bar/progress_bar'),
	MobileUsage: require('./patterns/usage/mobile'),
	VoiceMailPattern: require('./patterns/voicemail/voicemailinitial')
};