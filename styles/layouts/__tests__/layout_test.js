/**
 * Progress Bar Component
 * @jsx React.DOM
 */

jest.dontMock('../layout');
jest.dontMock('../column');

var Layout = require('../layout');
var Column = require('../column');

var React = require('react/react');
var TestUtils = React.addons.TestUtils;

var TestComponent = React.createClass({displayName: 'TestComponent',
	render : function (){
		return React.DOM.span(null, " Test content ")
	}
});

// Actual tests
describe('Layout 2 columns standard', function() {

     it('Should be a composite component', function() {
    	 var instance = Layout({fluid: "true"}, Column({xs: 12, sm: 8}, " ", TestComponent(null)))
    	 var isComposite = TestUtils.isCompositeComponent(instance);
    	 expect(isComposite).toBeTruthy();
     });
     
	it('Should be layout with container class for the reponsive layout', function(){
		var testObject = 	Layout({fluid: "false", className: "layout"}, 
								Column({xs: 12, className: "firstColumn"}, 
									TestComponent(null)
								), 
								Column({xs: 12, className: "secondColumn"}, 
									TestComponent(null)
								)
							) 
							
		TestUtils.renderIntoDocument(testObject);
		var instance = TestUtils.findRenderedDOMComponentWithClass(testObject, 'layout');
		expect(instance.getDOMNode().className.indexOf('container')).not.toBe(-1); 		
	});      
     
	it('Should be fluid layout with container-fluid class for the reponsive layout', function(){
		var testObject = 	Layout({fluid: "true", className: "layout"}, 
								Column({xs: 12, className: "firstColumn"}, 
									TestComponent(null)
								), 
								Column({xs: 12, className: "secondColumn"}, 
									TestComponent(null)
								)
							) 
							
		//console.log(React.renderComponentToString (testObject));

		TestUtils.renderIntoDocument(testObject);
		var instance = TestUtils.findRenderedDOMComponentWithClass(testObject, 'layout');
		expect(instance.getDOMNode().className.indexOf('container-fluid')).not.toBe(-1); 		
	});      


	it('Should be layout with two columns with width 12 on extra small devices for the reponsive layout', function(){
		var testObject = 	Layout({fluid: "false", className: "layout"}, 
								Column({xs: 12, className: "firstColumn"}, 
									TestComponent(null)
								), 
								Column({xs: 12, className: "secondColumn"}, 
									TestComponent(null)
								)
							) 
							
		TestUtils.renderIntoDocument(testObject);
		var firstColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'firstColumn');
		expect(firstColumn.getDOMNode().className.indexOf('col-xs-12')).not.toBe(-1); 		

		var secondColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'secondColumn');
		expect(secondColumn.getDOMNode().className.indexOf('col-xs-12')).not.toBe(-1); 		
	});      

	it('Should be layout with two columns with width 12 on small devices for the reponsive layout', function(){
		var testObject = 	Layout({fluid: "false", className: "layout"}, 
								Column({sm: 12, className: "firstColumn"}, 
									TestComponent(null)
								), 
								Column({sm: 12, className: "secondColumn"}, 
									TestComponent(null)
								)
							) 
							
		TestUtils.renderIntoDocument(testObject);
		var firstColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'firstColumn');
		expect(firstColumn.getDOMNode().className.indexOf('col-sm-12')).not.toBe(-1); 		

		var secondColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'secondColumn');
		expect(secondColumn.getDOMNode().className.indexOf('col-sm-12')).not.toBe(-1); 		
	});      

	it('Should be layout with two columns with width 6 each on medium devices for the reponsive layout', function(){
		var testObject = 	Layout({fluid: "false", className: "layout"}, 
								Column({md: 6, className: "firstColumn"}, 
									TestComponent(null)
								), 
								Column({md: 6, className: "secondColumn"}, 
									TestComponent(null)
								)
							) 
							
		TestUtils.renderIntoDocument(testObject);
		var firstColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'firstColumn');
		expect(firstColumn.getDOMNode().className.indexOf('col-md-6')).not.toBe(-1); 		

		var secondColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'secondColumn');
		expect(secondColumn.getDOMNode().className.indexOf('col-md-6')).not.toBe(-1); 		
	});      

	it('Should be layout with two columns one with width 4 and other with 8 each on large devices for the reponsive layout', function(){
		var testObject = 	Layout({fluid: "false", className: "layout"}, 
								Column({lg: 4, className: "firstColumn"}, 
									TestComponent(null)
								), 
								Column({lg: 8, className: "secondColumn"}, 
									TestComponent(null)
								)
							) 
							
		TestUtils.renderIntoDocument(testObject);
		var firstColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'firstColumn');
		expect(firstColumn.getDOMNode().className.indexOf('col-lg-4')).not.toBe(-1); 		

		var secondColumn = TestUtils.findRenderedDOMComponentWithClass(testObject, 'secondColumn');
		expect(secondColumn.getDOMNode().className.indexOf('col-lg-8')).not.toBe(-1); 		
	});
	      
});