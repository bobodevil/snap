/**
 * Progress Bar Component
 * @jsx React.DOM
 */

jest.dontMock('../column');

var Column = require('../column');

var React = require('react/react');
var TestUtils = React.addons.TestUtils;

var TestComponent = React.createClass({displayName: 'TestComponent',
	render : function (){
		return React.DOM.span(null, " Test content ")
	}
});

// Actual tests
describe('Columns standard Responsive', function() {
     it('Should be a composite component', function() {
    	 var testObject = Column({xs: 8, className: "columnComponent"}, " ", TestComponent(null))
    	 var isComposite = TestUtils.isCompositeComponent(testObject);
    	 expect(isComposite).toBeTruthy();
     });

	it('Should be a col-xs-12 class applied to column on small devices', function(){
		var testObject = Column({xs: 12, className: "columnComponent"}, " ", TestComponent(null))
		TestUtils.renderIntoDocument(testObject);
		var instance = TestUtils.findRenderedDOMComponentWithClass(testObject, 'columnComponent');
		expect(instance.getDOMNode().className.indexOf('col-xs-12')).not.toBe(-1); 		
	});      
     
	it('Should be a col-sm-12 class applied to column on small devices', function(){
		var testObject = Column({sm: 12, className: "columnComponent"}, " ", TestComponent(null))
		TestUtils.renderIntoDocument(testObject);
		var instance = TestUtils.findRenderedDOMComponentWithClass(testObject, 'columnComponent');
		expect(instance.getDOMNode().className.indexOf('col-sm-12')).not.toBe(-1); 		
	});      

	it('Should be col-md-6 class applied to column on small devices', function(){
		var testObject = Column({md: 6, className: "columnComponent"}, " ", TestComponent(null))
		TestUtils.renderIntoDocument(testObject);
		var instance = TestUtils.findRenderedDOMComponentWithClass(testObject, 'columnComponent');
		expect(instance.getDOMNode().className.indexOf('col-md-6')).not.toBe(-1); 		
	});      

	it('Should be col-lg-6 class applied to column on small devices', function(){
		var testObject = Column({lg: 6, className: "columnComponent"}, " ", TestComponent(null))
		TestUtils.renderIntoDocument(testObject);
		var instance = TestUtils.findRenderedDOMComponentWithClass(testObject, 'columnComponent');
		expect(instance.getDOMNode().className.indexOf('col-lg-6')).not.toBe(-1); 		
	});      

	it('Should be col-lg-6 class applied for large screen & col-md-12 for medium screen', function(){
		var testObject = Column({lg: 6, md: 12, className: "columnComponent"}, " ", TestComponent(null))
		TestUtils.renderIntoDocument(testObject);
		var instance = TestUtils.findRenderedDOMComponentWithClass(testObject, 'columnComponent');
		expect(instance.getDOMNode().className.indexOf('col-lg-6')).not.toBe(-1); 		
		expect(instance.getDOMNode().className.indexOf('col-md-12')).not.toBe(-1); 		
	});      

});
