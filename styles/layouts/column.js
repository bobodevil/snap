/**
 * Two column responsive layout
 * @jsx React.DOM
 */
 
var React = require('react/react');

/**
 * Column component 
 */


var Column = React.createClass({displayName: 'Column',

	propTypes: {
		xs				: React.PropTypes.oneOf([1,2,3,4,5,6,7,8,9,10,11,12]).isRequired,
		sm				: React.PropTypes.oneOf([1,2,3,4,5,6,7,8,9,10,11,12]).isRequired,
		md				: React.PropTypes.oneOf([1,2,3,4,5,6,7,8,9,10,11,12]).isRequired,
		lg				: React.PropTypes.oneOf([1,2,3,4,5,6,7,8,9,10,11,12]).isRequired,
	},

	getDefaultProps: function() {
		return {
			xs				: 0,
			sm				: 0,
			md				: 0,
			lg				: 0,
		};
	},
	
    render: function(){
    	
    	var applyClasses = {
    			xs: this.props.xs,
    			sm: this.props.sm,
    			md: this.props.md,
    			lg: this.props.lg
    	};
    	
    	var classSet = 	[];
    	
    	for(var prefix in applyClasses) {
    		if(applyClasses[prefix]) {
    			classSet.push('col-' + prefix + '-' + applyClasses[prefix])	
    		}
    	}
    	
    	var classSetString = classSet.join(' ');
    	
    	return  this.transferPropsTo(
    			React.DOM.div({className: classSetString}, 
                	this.props.children
                )
            );
    }
});


module.exports = Column;