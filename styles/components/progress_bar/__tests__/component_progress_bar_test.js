/**
 * @jsx React.DOM
 */

 jest.dontMock('../progress_bar');
 jest.dontMock('react/react');

var mock_data_100_alert = require('../__use_cases__/100_alert');
var mock_data_empty_success = require('../__use_cases__/empty_success');
var mock_data_half_full_warn = require('../__use_cases__/half_full_warn');


var React = require('react/react');
var TestUtils = React.addons.TestUtils;

var ProgressBar = require('../progress_bar');

describe('Empty success Progress Bar component', function() {

	var instance = ProgressBar(mock_data_empty_success);
    TestUtils.renderIntoDocument(instance);

    var progress = TestUtils.findRenderedDOMComponentWithClass(instance, 'progress-bar');

	it('Should be a composite component', function() {
		var isComposite = TestUtils.isCompositeComponent(instance);
		expect(isComposite).toBeTruthy();
	});


	it('Should have no progress displayed', function() {
	    expect(progress.getDOMNode().style.width).toBe('0%');
	});

	it('Should display the word "Empty"', function() {
	    expect(progress.getDOMNode().textContent).toBe('Empty');
	});

	it('Should be of colour success', function() {
	    expect(progress.getDOMNode().className.indexOf('success')).not.toBe(-1);
	});

});

describe('Half full warning Progress Bar component', function() {

	var instance = ProgressBar(mock_data_half_full_warn);
    TestUtils.renderIntoDocument(instance);

    var progress = TestUtils.findRenderedDOMComponentWithClass(instance, 'progress-bar');

	it('Should have a half way progress displayed', function() {
		expect(progress.getDOMNode().style.width).toBe('50%');
	});

	it('Should display the word "Half full"', function() {
		expect(progress.getDOMNode().textContent).toBe('Half full');
	});

	it('Should be of colour warn', function() {
		expect(progress.getDOMNode().className.indexOf('warn')).not.toBe(-1);
	});

});

describe('100% alert Progress Bar component', function() {

	var instance = ProgressBar(mock_data_100_alert);
    TestUtils.renderIntoDocument(instance);

    var progress = TestUtils.findRenderedDOMComponentWithClass(instance, 'progress-bar');

	it('Should have a full progress displayed', function() {
		expect(progress.getDOMNode().style.width).toBe('100%');
	});

	it('Should display the word "100%"', function() {
		expect(progress.getDOMNode().textContent).toBe('100%');
	});

	it('Should be of colour alert', function() {
		expect(progress.getDOMNode().className.indexOf('alert')).not.toBe(-1);
	});

});