/**
 * Documentation Live View
 * @jsx React.DOM
 */

var React = require('react/react');

var MOBILE_BREAKPOINT = 300;
var TABLET_BREAKPOINT = 750;
var DESKTOP_BREAKPOINT = 1200;

var DocLiveView = React.createClass({displayName: 'DocLiveView',
	
	getInitialState: function() {
		return {
			format: ''
		}
	},

	setFormat: function(format) {
		return this.setState({
			format: format
		});
	},
	propTypes: {
		children: React.PropTypes.component.isRequired
	},

	activeClass: function(format) {
		return this.state.format === format ? 'active': '';
	},

	render: function() {

		var containerStyle = {
			width: this.state.format
		};

		var classSet = 'doc-live-view ' + this.state.format;


		return React.DOM.div({className: classSet}, 
			React.DOM.div({className: "doc-live-view-sizes"}, 
				React.DOM.a({onClick: this.setFormat.bind(null, 'mobile'), className: 'fa fa-mobile ' + this.activeClass('mobile')}), 
				React.DOM.a({onClick: this.setFormat.bind(null, 'tablet'), className: 'fa fa-tablet ' + this.activeClass('tablet')}), 
				React.DOM.a({onClick: this.setFormat.bind(null, 'desktop'), className: 'fa fa-desktop ' + this.activeClass('desktop')})
			), 

			React.DOM.div({className: "doc-live-view-content"}, 
				this.props.children
			)
		)
	}

});

module.exports = DocLiveView;