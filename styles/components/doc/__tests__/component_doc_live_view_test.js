/**
 * @jsx React.DOM
 */
'use strict';

jest.dontMock('../doc_live_view');

var DocLiveView = require('../doc_live_view');
var use_case1 = require('../use_cases/sample_component.json');

var React = require('react/react');
var TestUtils = React.addons.TestUtils;


var NestedMockedComponent = React.createClass({displayName: 'NestedMockedComponent',
	render: function() {
		return React.DOM.h1({className: "happy-component"}, "Happy Days!")
	}
});


describe('Documentation: Live View', function() {

	var instance = DocLiveView(null, 
		NestedMockedComponent(null)
	);

	TestUtils.renderIntoDocument(instance);

	it('Should display the nested (live) component', function() {
		var happyComponent = TestUtils.findRenderedDOMComponentWithClass(instance, 'happy-component');
		expect(happyComponent.getDOMNode().textContent).toBe('Happy Days!');
	});

	it('Should change size when user click on the desktop button', function() {

		var buttons = TestUtils.scryRenderedDOMComponentsWithTag(instance, 'a');
		
		// Find desktop button
		// var desktopButton = buttons.filter(function(button) {
		// 	return button.getDOMNode().textContent.toLowerCase().indexOf('desktop') !== -1;
		// })[0];

		// Click on first button (mobile)
		React.addons.TestUtils.Simulate.click(buttons[0]);

		// Expect format state to change
		expect(instance.state.format).toBe('mobile');

		// And it should apply to the actual DOM node too!
		expect(instance.getDOMNode().className.indexOf('mobile')).not.toBe(-1);

	});

});