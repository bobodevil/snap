/**
 * Mock for doc live view
 * @jsx React.DOM
 */
'use strict';

var React = require('react/react');

module.exports = React.createClass({displayName: 'exports',
	render: function() {
		return React.DOM.div({className: "doc-live-view"}, this.props.children)
	}
})