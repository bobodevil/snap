/**
 * Documentation rendering entry file
 * @jsx React.DOM
 */

require('./main');

var React = require('react');
var Doc = require('./components/doc/doc');
var components = require('./components/doc/use_cases/sample_component_list.json');
var VoiceMailPattern = require('./patterns/voicemail/voicemailinitial');


React.renderComponent(VoiceMailPattern(model), document.getElementById('widget-container'));